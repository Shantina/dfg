$(function () {

	//write your code here
	var raceBtn = $('.raceBtn');
	var startBtn = $('.startBtn');
	var car = $('.car');
	var pinkCar = $('.pink-car');
	var redCar = $('.red-car');
	var raceTrack = $('.race-track');
	var result1 = $('.results1');
	var result2 = $('.results2');
	var carWidth = $('.car').width();
	var track = $(window).width() - carWidth;
	let brojac = 0;
	function animateSprite() {
		$('.counting').eq(0).animate({
			opacity: '1'
		},
			{
				duration: 1000,
				complete: function () {
					$('.counting').eq(1).animate({
						opacity: '1'
					}),
						$('.counting').eq(0).animate({
							opacity: '0'
						},
							{
								duration: 1000,
								complete: function () {
									$('.counting').eq(2).animate({
										opacity: '1'
									}),
										$('.counting').eq(1).animate({
											opacity: '0'
										},
											{
												duration: 1000,
												complete: function () {
													$('.counting').eq(2).animate({
														opacity: '0'
													})
												}
											}
										)

								}
							}

						)
				}
			})

		raceTrack.delay(3000).animate({ opacity: '1' })
		var minSpeed = 1000;

		var pinkCarSpeed = parseInt(minSpeed + Math.random() * 1000, 10);
		var redCarSpeed = parseInt(minSpeed + Math.random() * 1000, 10);
		var x;
		var y;
		function winner() {
			if (pinkCarSpeed < redCarSpeed) {
				x = "first"
				y = "second"
			} else {
				x = "second"
				y = "first"
			}
		}
		pinkCar.delay(3000).animate({ left: track }, pinkCarSpeed,
			function () {
				winner()
				result1.append(`<tr class="row-car"><td class="colorText1">Finished in: <span class="span text-white"> ${x} </span> place with a time of: <span class="span text-white"> ${pinkCarSpeed} </span> milliseconds!</td></tr>`)
				brojac++
				if (brojac === 2) {
					$('.raceBtn').attr("disabled", false);
					$('.startBtn').attr("disabled", false);
				}
			})
		redCar.delay(3000).animate({ left: track }, redCarSpeed,
			function () {
				winner()
				result2.append(`<tr class="row-car1"><td class="colorText1">Finished in:  <span class="span text-danger"> ${y} </span> place with a time of: <span class="span text-danger"> ${redCarSpeed} </span> milliseconds!</td></tr>`)
				brojac++
				if (brojac === 2) {
					$('.raceBtn').attr("disabled", false);
					$('.startBtn').attr("disabled", false);
				}
			})

		// var table = $('table')
		setTimeout(function () {
			if (pinkCarSpeed < redCarSpeed) {
				raceTrack.css('opacity', '0.5');
				$(".annouce").animate({ opacity: 1 })
				result1.animate({
					opacity: 1
				});
				result2.animate({
					opacity: 1
				});
			} else {
				raceTrack.css('color', '0,0,0,0.5');
				raceTrack.css('opacity', '0.5');
				$(".annouce").animate({ opacity: 1 },
				)
				result1.animate({
					opacity: 1
				});
				result2.animate({
					opacity: 1
				});
			}
		}, Math.min.apply(Math, [pinkCarSpeed, redCarSpeed]) + 3000);


		localStorage.setItem("CAR1value", $('.results1').find('.colorText1').eq(0).html());
		localStorage.getItem("CAR1value");
		localStorage.setItem("CAR2value", $('.results2').find('.colorText1').eq(0).html());
		localStorage.getItem("CAR2value");

	};

	raceBtn.on('click', function () {
		animateSprite();
		$('.raceBtn').attr("disabled", true);
		$('.startBtn').attr("disabled", true);
	});
	startBtn.on('click', function () {
		$(".annouce").animate({ opacity: 0 })
		pinkCar.css({ left: 0 })
		redCar.css({ left: 0 })

		$('.startBtn').attr("disabled", true);
	});

	localStorage.setItem("header", 'Results from the previous time you played this game:')
	localStorage.getItem("header")
	$('.header-results').append(localStorage.getItem("header"))

	$('.td1').append(localStorage.getItem("CAR1value"))
	$('.td2').append(localStorage.getItem("CAR2value"))
});






